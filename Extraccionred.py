#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 18:35:38 2021

@author: jj
"""

from __future__ import absolute_import
from __future__ import print_function

import os
import sys
import optparse
import random

# we need to import python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable Juan 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa

import math
import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
%matplotlib inline
from collections import namedtuple
from itertools import count
from PIL import Image
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
from random import randrange, uniform
import itertools
# %% ## Funcion para calcular los semáforos que hay que cambiar, la lista y las variables.

def run():
    """execute the TraCI control loop"""
    step = 0
    print("Hola")
    while traci.simulation.getMinExpectedNumber() > 0:
        print("Hola1")
        traci.simulationStep()

        step += 1
    traci.close()
def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options

class Extr(nn.Module):
    
   def __init__(self,sem):
       super().__init__()
       self.device= torch.device("cuda" if torch.cuda.is_available() else "cpu")
       self.sem=sem #Pasamos la interseccion a la clase
       self.vinc=[] # Vecinos incidentes
       for i in jm[df.loc[df['id']==self.sem.attrib['id']].index[0],:].nonzero()[1]:
           if i<df_tl.shape[0]:
               self.vinc.append(df_tl.iloc[i][0])
       ## Obtenemos las fases de los otros semaforos         
       self.L()
       self.accionelegida={}
       self.ultimoestado=list(self.Listaytipo().items())[-1][0]
       self.Acciones()
       ## Aproximador
       self.fc1 = nn.Linear(in_features=len(self.fases), out_features=24)  
       self.fc2 = nn.Linear(in_features=24, out_features=32)
       self.out = nn.Linear(in_features=32, out_features=len(self.accionred))
       
       a=self.Listaytipo()
       del a[self.ultimoestado]
       self.estado=self.Listaytipo()
       self.Acciones()
      
           
       #     self.estado.append(selfsita)
       # in_features=

## Forward propagation red       
   def forward(self, t):
       t = t.flatten()
       t = F.relu(self.fc1(t))
       t = F.relu(self.fc2(t))
       t = self.out(t)     
       return t    
## Extracciones de fases que cambiar, ya que hay algunas que son necesarias para mantener la seguridad  
# También se extrae la longitud del ciclo                                  
   def Listaytipo(self):
        d1={};
        i=0;
        d1.update({"offset":float(self.sem.get("offset"))})
        self.ciclo=0
        for fase in self.sem.iter('phase'):
            self.ciclo+=float(fase.get("duration"))
            if float(fase.get("duration"))>9:
                d1.update({fase.get('state'):float(fase.get("duration"))})
                           
            i=i+1
        return d1
##  Extraccion de fases vecinas y propias.
   def L(self):
        self.fases=[]
        self.fases.append(float(self.sem.get("offset")))
        for fase in self.sem.iter('phase'):
            if float(fase.get("duration"))>9:
                self.fases.append(float(fase.get("duration")))
        for i in self.vinc:
            a='./tlLogic[@id="'+i+'"]'
            a1=root.findall(a)[0]
            self.fases.append(float(a1.get("offset")))
            for fase in a1.iter('phase'):
                if float(fase.get("duration"))>9:
                    self.fases.append(float(fase.get("duration")))
## Extraccion de acciones posibles. Estas vienen dadas en forma de lista, donde cada entrada es un diccionario, el cual contine 
# suma que se le tiene que hacer a cada fase, menos a la ultima.
   def Acciones(self):
       a=self.Listaytipo()
       del a[self.ultimoestado]
       ok=a
       for b in ok:
           ok[b]=[ok[b],[-1/2,0,1/2]]
       self.accio=ok
       self.acciored={}
       l=0
       m=0
       o=l
       op=pow(3,len(self.accio))-1
       opb3=len(np.base_repr(op,base=3))
       b={}
       v=[]
       lacc=list(self.accio)
       for i in range(op+1):
           
           k=np.base_repr(i,base=3,padding=opb3-len(np.base_repr(i,base=3))+2)[::-1]
           for j in range(opb3):
               b.update({lacc[j]:[-0.5,0,0.5][int(k[j])]  })           
           v.append(b)
           
           b={}
       self.accionred=v 
       # v=list(self.accio)
       # for i in range(op+1):
           
       #     k=np.base_repr(i,base=3,padding=opb3-len(np.base_repr(i,base=3)))
       #     for j in range(opb3):
       #         b.update({v[j]:[-0.5,0,0.5][int(k[j])]  })
               
            
       #     self.acciored.update({i:b})
       #     b={}

           
## Cambiar el estado. Primero z                
   def Efectuaraccion(self):
       x=0
       for i in self.accio:
           if i=="offset":
               q=self.accionelegida[i]
               self.estado[i]=((q+self.estado[i]) % self.ciclo)
               
           else:
               q=self.accionelegida[i]
               x=x+q
               self.estado[i]=((q+self.estado[i])% 120)
       self.estado[self.ultimoestado]=self.estado[self.ultimoestado]-x 
       
            # Posible bug numeros negativos.
   def inicio(self, strategy, num_actions, device):
       self.current_step = 0
       self.strategy = strategy
       self.num_actions = num_actions
       self.device = device
   
   def select_action(self):
       rate = 0
       self.current_step = 1
       if rate > random.random():
           for i in self.accio:
               self.accionelegida=self.acciored[randrange(len(self.acciored))] # explore      
       else:
           with torch.no_grad():
               self.accionelegida=self.accionred[int(self.forward(torch.FloatTensor(self.fases)).argmax().to(self.device))] # exploit
               #self.accionelegida=self.acciored[int(policy_net(state).argmax(dim=1).to(self.device)[0])] # exploit
               
   def get_options():
      optParser = optparse.OptionParser()
      optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
      options, args = optParser.parse_args()
      return options   
    
   def Training(self):
       print(self.estado)
       self.select_action()
       self.Efectuaraccion()
       
       print(self.estado)
               
   def run(self):
       
        if __name__ == "__main__":
            options = get_options()
        
            # this script has been called from the command line. It will start sumo as a
            # server, then connect and run
            if options.nogui:
                print("nogui")
                sumoBinary = checkBinary('sumo')
            else:
                print("gui")
                sumoBinary = checkBinary('sumo-gui')
        
            # first, generate the route file for this simulation
        
            # this is the normal way of using traci. sumo is started as a
            # subprocess and then the python script connects and runs
            traci.start(["sumo", "-c", "sim.sumocfg",
                                     "--tripinfo-output", "tripinfo.xml"])
            step = 0
            print(traci.simulation.getMinExpectedNumber())
            while 250 > step:
                traci.simulationStep()
        
                step += 1
            traci.close()
            
 # %% 
l=Extr(root.findall(a)[0])           
# print(l.select_action(),l.accionelegida, l.estado)
l.Training()
l.run()
            
            
# %%
Sems={}
for i in df_tl["id"]:
    a='./tlLogic[@id="'+i+'"]'
    Sems.update({i:Extr(root.findall(a)[0])})
    Sems[i].Training()

    
# %% 

class EpsilonGreedyStrategy():
    def __init__(self, start, end, decay):
        self.start = start
        self.end = end
        self.decay = decay
        
    def get_exploration_rate(self, current_step):
        return self.end + (self.start - self.end) * \
        math.exp(-1. * current_step * self.decay)
    