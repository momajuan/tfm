#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 17:48:37 2021

@author: jj
"""
sumo()
step = 0
while 250 > step:
    traci.simulationStep()
    step += 1
    print(traci.trafficlight.getPhaseDuration('gneJ45'))
print("End")
# %%
phases = [] 
phases.append(traci.trafficlight.Phase(30, "GGGrrrGGG")) 
phases.append(traci.trafficlight.Phase(10, "yyyrrryyy")) 
phases.append(traci.trafficlight.Phase(40, "rrrGGGrrr")) 
phases.append(traci.trafficlight.Phase(20, "rrryyyrrr")) 
logic = traci.trafficlight.Logic("custom", 0, 0, phases) 
traci.simulationStep()

print(traci.trafficlight.getPhaseName('gneJ45'))
print(traci.trafficlight.getNextSwitch('gneJ45'))

print(traci.trafficlight.getCompleteRedYellowGreenDefinition('gneJ45'))
print(traci.trafficlight.setCompleteRedYellowGreenDefinition('gneJ45',logic))

print(traci.trafficlight.getPhaseDuration('gneJ45'))
print(traci.trafficlight.getProgram('gneJ45'))
# %%
step = 0
while 250 > step:
    traci.simulationStep()
    step += 1
    print(traci.trafficlight.getPhaseDuration('gneJ45'))
print("End")