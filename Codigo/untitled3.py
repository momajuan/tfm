import concurrent.futures
import math

PRIMES = [
    112272535095293,
    112582705942171,
    112272535095293,
    115280095190773,
    115797848077099,
    1099726899285419]

def is_prime(n):
    if n < 2:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False

    sqrt_n = int(math.floor(math.sqrt(n)))
    for i in range(3, sqrt_n + 1, 2):
        if n % i == 0:
            return False
    return True

def sumo():
     if __name__ == "__main__":
         options = get_options()
     
         # this script has been called from the command line. It will start sumo as a
         # server, then connect and run
         if options.nogui:
             print("nogui")
             sumoBinary = checkBinary('sumo')
         else:
             print("gui")
             sumoBinary = checkBinary('sumo-gui')
     
         # first, generate the route file for this simulation
     
         # this is the normal way of using traci. sumo is started as a
         # subprocess and then the python script connects and runs
         traci.start(["sumo", "-c", "C:/Users/juano/Documents/Tfm/Tfm/sim.sumocfg",
                                  "--tripinfo-output", "tripinfo.xml"])
         step = 0
         while 700> step:
             traci.simulationStep()
             for i in df_tl["id"]:
                 Sems[i].Tiempo_espera_interseccion()
     
             step += 1
     traci.close()  
     
k=2

def sumo1(conection):
     k=3
     return k
     if __name__ == "__main__":
         print("a")
         options = get_options()
     
         # this script has been called from the command line. It will start sumo as a
         # server, then connect and run
         if options.nogui:
             print("nogui")
             sumoBinary = checkBinary('sumo')
         else:
             print("gui")
             sumoBinary = checkBinary('sumo-gui')
     
         # first, generate the route file for this simulation
     
         # this is the normal way of using traci. sumo is started as a
         # subprocess and then the python script connects and runs
         print("a")
         traci.start(["sumo-gui", "-c", "C:/Users/juano/Documents/Tfm/Tfm/sim.sumocfg",
                                  "--tripinfo-output", "tripinfo.xml"],label=conection)
         con=traci.getConnection(conection)
         step = 0
         while 700> step:
             con.simulationStep()
             for i in df_tl["id"]:
                 Sems[i].Tiempo_espera_interseccion()
     
             step += 1
             print(step)
     conection.close()
def main():
    with concurrent.futures.ProcessPoolExecutor() as executor:
        print([i for i in executor.map(sumo1, ["2"])])

if __name__ == '__main__':
    main()