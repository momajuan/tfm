# -*- coding: utf-8 -*-
import time
import concurrent.futures


"""
Created on Wed Sep  1 10:48:24 2021

@author: juano
"""
# %% Parametros
batch_size = 1
gamma = 0.999
eps_start = 1
eps_end = 0.05
eps_decay = 0.0005
target_update = 10
memory_size = 100000
lr = 0.001
num_episodes = 10000

Experience = namedtuple(
    'Experience',
    ('state', 'action', 'next_state', 'reward')
)
class EpsilonGreedyStrategy():
    def __init__(self, start, end, decay):
        self.start = start
        self.end = end
        self.decay = decay
        
    def get_exploration_rate(self, current_step):
        return self.end + (self.start - self.end) * \
        math.exp(-1. * current_step * self.decay)
getrate=EpsilonGreedyStrategy(eps_start,eps_end,eps_decay)

# %%%  Elegir accion para todas las clases. Primero hace falta hacer las particiones del grafo.
def Accioncolor(color):
    colors=color % len(coloringlist)
    if color %2 ==0:
        for i in coloringlist[colors]:
            Sems[i].select_action(Paccion[0])
            Sems[i].Efectuaraccion()
            print(i, Sems[i].estado)
            Sems[i].sem.set("offset", str(Sems[i].estado["offset"]))
            for j in Sems[i].sem.iter("phase"):
                if j.attrib["state"] in Sems[i].estado:
                    j.set("duration", str(Sems[i].estado[j.attrib["state"]]))
        dom.write("/home/juan/Documentos/Tfm/Codigo/d"+str(color)+".net.xml")
    else:
        for i in coloringlist[colors]:
            Sems[i].select_action(Paccion[1])
            Sems[i].Efectuaraccion()
            Sems[i].sem.set("offset", str(Sems[i].estado["offset"]))
            for j in Sems[i].sem.iter("phase"):
                if j.attrib["state"] in Sems[i].estado:
                    j.set("duration", str(Sems[i].estado[j.attrib["state"]]))
        dom.write("/home/juan/Documentos/Tfm/Codigo/d"+str(color)+".net.xml")    
def sumo(conection,coloracion):
    # print("Hello")
    # time.sleep(1)
    a={}
    traci.start(["sumo", "-c", "/home/juan/Documentos/Tfm/sim"+str(coloracion)+".sumocfg",
                                  "--tripinfo-output", "tripinfo.xml"],label=conection)
    con=traci.getConnection(conection);
    step = 0
    a={}
    while 700> step:
        con.simulationStep();
        step += 1
        for i in df_tl["id"]:
            Sems[i].Tiempo_espera_interseccion();
            
    for i in Sems:
        a.update({i:Sems[i].tiempoespera});

    traci.switch(conection)
    traci.close()
    return {coloracion:a};
        
# def sumo():
#       if __name__ == "__main__":
#           options = get_options()
     
#           # this script has been called from the command line. It will start sumo as a
#           # server, then connect and run
#           if options.nogui:
#               print("nogui")
#               sumoBinary = checkBinary('sumo')
#           else:
#               print("gui")
#               sumoBinary = checkBinary('sumo-gui')
     
#           # first, generate the route file for this simulation
     
#           # this is the normal way of using traci. sumo is started as a
#           # subprocess and then the python script connects and runs
#           traci.start(["sumo", "-c", "/home/juan/Documentos/Tfm/sim.sumocfg",
#                                   "--tripinfo-output", "tripinfo.xml"])
#           step = 0
#           while 700> step:
#               traci.simulationStep()
#               for i in df_tl["id"]:
#                   Sems[i].Tiempo_espera_interseccion()
                 
     
#               step += 1
#       traci.close()

def extract_tensors(experiences):
    # Convert batch of Experiences to Experience of batches
    batch = Experience(*zip(*experiences))

    t1 = torch.cat(batch.state)
    t2 = torch.cat(batch.action)
    t3 = torch.cat(batch.reward)
    t4 = torch.cat(batch.next_state)

    return (t1,t2,t3,t4)
class QValues():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    @staticmethod
    def get_current(policy_net, states, actions):
        return policy_net(states).gather(dim=1, index=actions.unsqueeze(-1))
    @staticmethod        
    def get_next(target_net, next_states):                
        final_state_locations = False
        non_final_state_locations = (final_state_locations == False)
        non_final_states = next_states[non_final_state_locations]
        batch_size = next_states.shape[0]
        values = torch.zeros(batch_size).to("cuda")
        values[non_final_state_locations] = target_net(non_final_states).max(dim=1)[0].detach()
        return values
    # %% Trainging loop.
memory = ReplayMemory(memory_size)
episode_durations = []
label=[]
values=[]
for k in range(4*len(coloringlist)):
    label.append((str("a"+str(k)),k,))
st = time.time()

for k in range(4*len(coloringlist)):
    file_namecfg='/home/juan/Documentos/Tfm/sim.sumocfg'
    domcfg=ET.parse(file_namecfg)
    rootcfg = domcfg.getroot()
    for i in rootcfg.iter("net-file"):
        i.set("value", "/home/juan/Documentos/Tfm/Codigo/d"+str(k)+".net.xml")
    domcfg.write('/home/juan/Documentos/Tfm/sim'+str(k)+'.sumocfg')  
fases=[]
ayuda={}
for episode in range(20):
    for timestep in range(10):
        rate=getrate.get_exploration_rate(12*len(coloringlist)*(episode*20+timestep*4*len(coloringlist)))
        for k in range(4*len(coloringlist)):
            Accioncolor(k)
            file_name='/home/juan/Documentos/Tfm/Codigo/d'+str(k)+'.net.xml'
            dom=ET.parse(file_name)
            root = dom.getroot()

            for i in df_tl["id"]:
                a='./tlLogic[@id="'+i+'"]'
                Sems[i].sem=root.findall(a)[0]
    
            for i in Sems: 
                Sems[i].L()
                Sems[i].tiempoespera=0
                ayuda.update({i:Sems[i].fases})
            fases.append({k:ayuda})
            #sumo()
        print(Sems[i].fases)
        time.sleep(1)
        def helper(numbers):
            return sumo(numbers[0], numbers[1]);
        with concurrent.futures.ProcessPoolExecutor() as executor:
            for result in executor.map(helper, label):
                values.append(result);
        time.sleep(3)
        print(values)
        time.sleep(4)
        for k in range(4*len(coloringlist)):
            if k%2==0:
                print(k)
                for i in values[k][k]:
                    Sems[i].tiempoespera=values[k][k][i]
                    Sems[i].fases=fases[k][k][i]
                for j in coloringlist[k % len(coloringlist)]:
                    Redes[str(Paccion[k%2])][j].memory.push(Experience(torch.FloatTensor([Sems[j].fasesant]).to("cuda"),torch.FloatTensor( [Sems[j].action]).type(torch.int64).to("cuda"), torch.FloatTensor([Sems[j].fases]).to("cuda"),torch.FloatTensor( [Sems[j].Recompensa()]).to("cuda")))
                    Sems[j].fasesant=Sems[j].fases
                    if Redes[str(Paccion[k%2])][j].memory.can_provide_sample(batch_size):
                        experiences = Redes[str(Paccion[k%2])][j].memory.sample(batch_size)
                        states, actions, rewards, next_states = extract_tensors(experiences)
                        current_q_values = QValues.get_current(Redes[str(Paccion[k%2])][j], states, actions)
                        next_q_values = QValues.get_next(Targets[str(Paccion[k%2])][j], next_states)
                        target_q_values = (next_q_values * gamma) + rewards
                        Redes[str(Paccion[k%2])][j].loss = F.mse_loss(current_q_values, target_q_values.unsqueeze(1))
                        Redes[str(Paccion[k%2])][j].optimizer.zero_grad()
                        Redes[str(Paccion[k%2])][j].loss.backward()
                        Redes[str(Paccion[k%2])][j].optimizer.step()
                    if episode % target_update == 0:
                        Redes[str(Paccion[k%2])][j].load_state_dict(Redes[str(Paccion[k%2])][j].state_dict())
            elif k%2==0:
                for i in values[k][k]:
                    Sems[i].tiempoespera=values[k][k][i]
                    Sems[i].fases=fases[k][k][i]
                for j in coloringlist[k % len(coloringlist)]:
                    Redes[str(Paccion[k%2])][j].memory.push(Experience(torch.FloatTensor([Sems[j].fasesant]).to("cuda"),torch.FloatTensor( [Sems[j].action]).type(torch.int64).to("cuda"), torch.FloatTensor([Sems[j].fases]).to("cuda"),torch.FloatTensor( [Sems[j].Recompensa()]).to("cuda")))
                    Sems[j].fasesant=Sems[j].fases
                    if Redes[str(Paccion[k%2])][j].memory.can_provide_sample(batch_size):
                        experiences = Redes[str(Paccion[k%2])][j].memory.sample(batch_size)
                        states, actions, rewards, next_states = extract_tensors(experiences)
                        current_q_values = QValues.get_current(Redes[str(Paccion[k%2])][j], states, actions)
                        next_q_values = QValues.get_next(Targets[str(Paccion[k%2])][j], next_states)
                        target_q_values = (next_q_values * gamma) + rewards
                        Redes[str(Paccion[k%2])][j].loss = F.mse_loss(current_q_values, target_q_values.unsqueeze(1))
                        Redes[str(Paccion[k%2])][j].optimizer.zero_grad()
                        Redes[str(Paccion[k%2])][j].loss.backward()
                        Redes[str(Paccion[k%2])][j].optimizer.step()
                    if episode % target_update == 0:
                        Targets[str(Paccion[k%2])][j].load_state_dict(Redes[str(Paccion[k%2])][j].state_dict())
        values=[    ]
        fases=[]
        ayuda={}


# label=[]
# for k in coloringlist:
#     label.append(("a"+str(k),"b"+str(k)))
            
# def helper(numbers):
#     return sumo(numbers[0], numbers[1])
# values=[]
# label=(("sim1",0),("sim2",1),("sim3",2))
# if __name__ == '__main__':
#     with concurrent.futures.ProcessPoolExecutor() as executor:
#                 for result in executor.map(helper, label):
#                     values.append(result)
fn = time.time()
fn= time.time()
# %% 

print(fn-st)

# %% 

            
            
            
            
            # %%
            
            
            
            
            
            