# %%
import os
import pandas as pd
import numpy as np
from xml.etree import ElementTree as ET
# Import required package
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
import networkx as nx
from networkx.algorithms.community import greedy_modularity_communities
import matplotlib.pyplot as plt 


# %%
file_name='/home/juan/Documentos/Tfm/Mapa1.net.xml'
dom=ET.parse(file_name)
root = dom.getroot()
df=pd.DataFrame(columns=['id','x','y'])
junction=pd.DataFrame(columns=['id','x','y'])

# %%
## Junctions
v=root.findall('./junction[@shape]')
for elm in v:
    junction=junction.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)

## Traffic light
v=root.findall('./junction[@type="traffic_light"]')
for elm in v:
    df=df.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
df_tl=df.copy()
for elm in root.findall('./edge[@to]'):
    if df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty  == False:
        df=df.append({'id': elm.attrib["from"]}, ignore_index=True)
df=df.drop_duplicates(subset=['id'], keep='first')
df=df.reset_index()



m=df_tl.shape[0]
n=df.shape[0]
Matriz_semaforos = csr_matrix((m,m ),dtype = np.int8).toarray()
Matriz_semaforos=coo_matrix(Matriz_semaforos)
Matriz_semaforos=Matriz_semaforos.tocsr()

for elm in root.findall('./edge[@to]'):
    if df_tl.loc[df_tl['id'] == elm.attrib["from"]].empty == False and df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty==False:

        Matriz_semaforos[ df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0],df_tl.loc[df_tl['id']==elm.attrib["to"]].index[0]]=1
        # js[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
        # jl[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
        # jv[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
        # jc[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())

# %%
##Añadir x, y
for i in list(df.id):
    df.loc[df['id']==i,'x':'y']=junction.loc[junction['id']==i,'x'].iloc[0],junction.loc[junction['id']==i,'y'].iloc[0]
## Matriz de Conexion de intersecciones
JM = csr_matrix((n, n),dtype = np.int8).toarray()
## Matriz de conexion con numero de carriles
JC = csr_matrix((n, n),dtype = np.int8).toarray()
## Matriz de posicion de semaforo en la intersecciones
JS = csr_matrix((n, n),dtype = np.int8).toarray()
## Matriz de longitud de segmento
JL = csr_matrix((n, n),dtype = np.float).toarray()
## Matriz de velocidad
JV = csr_matrix((n, n),dtype = np.float).toarray()
## Matriz de velocidad
JI = csr_matrix((n, n),dtype = np.float).toarray()
JM=coo_matrix(JM)
JC=coo_matrix(JC)
JS=coo_matrix(JS)
JL=coo_matrix(JL)
JV=coo_matrix(JV)
JI=coo_matrix(JI)
jm=JM.tocsr()
js=JS.tocsr()
jl=JL.tocsr()
jv=JV.tocsr()
ji=JI.tocsr()
jc=JC.tocsr()
dedges=pd.DataFrame(columns=['id','from','to','index'])
for elm in root.findall('./edge[@from]'):
    dedges=dedges.append({'id': elm.attrib["id"],'from':elm.attrib["from"],'to':elm.attrib["to"],'index':[]}, ignore_index=True)
# %%
for elm in root.findall('./edge[@to]'):
    if df.loc[df['id'] == elm.attrib["from"]].empty == False and df.loc[df['id'] == elm.attrib["to"]].empty==False:

        jm[ df.loc[df['id']==elm.attrib["from"]].index[0],df.loc[df['id']==elm.attrib["to"]].index[0]]=1
        # js[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
        # jl[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
        # jv[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
        # jc[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())

# %%
## Aristas, de donde viene a donde van y indice de semaforo que las une
for elm in root.findall('./connection[@tl]'):
    dedges.iloc[dedges.iloc[(dedges['id'] == elm.attrib["from"]).values,[3]].index[0]][3].append(elm.attrib["linkIndex"])

# %%
###
dedges.iloc[1][3].append(1)
def inverse_mapping(f):
    return f.__class__(map(reversed, f.items()))

G=nx.from_scipy_sparse_matrix(Matriz_semaforos, parallel_edges=False, create_using=nx.DiGraph)
Colors={}
listcoloring=nx.coloring.greedy_color(G)
b=nx.coloring.greedy_color(G)
for k in b:
    listcoloring[df_tl.iloc[k][0]]=listcoloring.pop(k)

# %%
coloringlist = {}
for k, v in listcoloring.items():
    coloringlist[v] = coloringlist.get(v, []) + [k]


