#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 18:35:38 2021

@author: jj
"""
from __future__ import absolute_import
from __future__ import print_function

import os
import sys
import optparse
import random

# we need to import python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable Juan 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa

import math
import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt 
from collections import namedtuple
from itertools import count
from PIL import Image
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
from random import randrange, uniform
import itertools
# %% ## Funcion para calcular los semáforos que hay que cambiar, la lista y las variables.
lr=0.001
device="cuda"
def run():
    """execute the TraCI control loop"""
    step = 0
    print("Hola")
    while traci.simulation.getMinExpectedNumber() > 0:
        print("Hola1")
        traci.simulationStep()

        step += 1
    traci.close()
def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


class ReplayMemory():
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.push_count = 0 # How many exps have been added to memo

    def can_provide_sample(self, batch_size):
        return len(self.memory) >= batch_size

    def push(self, experience):
        if len(self.memory) < self.capacity:
            self.memory.append(experience)
        else:
            self.memory[self.push_count % self.capacity] = experience
        self.push_count += 1
    
    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)
class Target(nn.Module):
       
   def __init__(self,m,n):
       super().__init__()
       self.device= torch.device("cuda" if torch.cuda.is_available() else "cpu")
       self.memory=ReplayMemory(10000)


       ## Aproximador
       self.fc1 = nn.Linear(in_features=m, out_features=2*m)  
       self.fc2 = nn.Linear(in_features=2*m, out_features=2*n)
       self.out = nn.Linear(in_features=2*n, out_features=n)
       self.optimizer = optim.Adam(params=self.parameters(), lr=lr)

   def forward(self, t):
       t = t.flatten(start_dim=1)
       t = F.relu(self.fc1(t))
       t = F.relu(self.fc2(t))
       t = self.out(t)     
       return t    
## Extracciones de 
           
    
class Extr(nn.Module):
    
   def __init__(self,sem,desfa):
       super().__init__()
       self.device= torch.device("cuda" if torch.cuda.is_available() else "cpu")
       self.sem=sem #Pasamos la interseccion a la clase
       self.nombre=self.sem.attrib["id"]
       self.vinc=[] # Vecinos incidentes
       for i in jm[:,df.loc[df['id']==self.sem.attrib['id']].index[0]].nonzero()[0]:
           if i<df_tl.shape[0]:
               self.vinc.append(df_tl.iloc[i][0])
              
       self.vout=[] # Vecinos incidentes
       for i in jm[df.loc[df['id']==self.sem.attrib['id']].index[0],:].nonzero()[1]:
           if i<df_tl.shape[0]:
               self.vout.append(df_tl.iloc[i][0])
       ## Obtenemos las fases de los otros semaforos         
       self.L()
       self.accionelegida={}
       self.ultimoestado=list(self.Listaytipo().items())[-1][0]
       self.Acciones(desfa)
       ## Aproximador
       self.fc1 = nn.Linear(in_features=len(self.fases), out_features=2*len(self.fases))  
       self.fc2 = nn.Linear(in_features=2*len(self.fases), out_features=2*len(self.accionred))
       self.out = nn.Linear(in_features=2*len(self.accionred), out_features=len(self.accionred))
       
       self.Prog()
       self.estado=self.Listaytipo()
       self.Acciones(desfa)
       self.tiempoespera=0
       self.tecoloracion={}
       self.Carreteras()
       self.memory=ReplayMemory(10000)
       self.fasesant=self.fases
       self.optimizer = optim.Adam(params=self.parameters(), lr=lr)
    
       #     self.estado.append(selfsita)
       # in_features=

## Forward propagation red       
   def forward(self, t):
       t = t.flatten(start_dim=1)
       t = F.relu(self.fc1(t))
       t = F.relu(self.fc2(t))
       t = self.out(t)     
       return t    
## Extracciones de fases que cambiar, ya que hay algunas que son necesarias para mantener la seguridad  
# También se extrae la longitud del ciclo                                  
   def Listaytipo(self):
        d1={};
        i=0;
        d1.update({"offset":float(self.sem.get("offset"))})
        self.ciclo=0
        self.programa=[]
        self.programacambios=[]
        for fase in self.sem.iter('phase'):
            self.ciclo+=float(fase.get("duration"))
            if float(fase.get("duration"))>9:
                d1.update({fase.get('state'):float(fase.get("duration"))})
                self.programacambios.append(i)
                           
            i=i+1
        return d1        
   def Prog(self):
        i=0
        for fase in self.sem.iter('phase'):
            self.programa.append({fase.get('state'):float(fase.get("duration"))})
           
   def Mat(self):
       return np.random.rand(m_size, m_size)
        

##  Extraccion de fases vecinas y propias.
   def L(self):
        self.fases=[]
        self.fases.append(float(self.sem.get("offset")))
        for fase in self.sem.iter('phase'):
            if float(fase.get("duration"))>9:
                self.fases.append(float(fase.get("duration")))
        for i in self.vinc:
            a='./tlLogic[@id="'+i+'"]'
            a1=root.findall(a)[0]
            self.fases.append(float(a1.get("offset")))
            for fase in a1.iter('phase'):
                if float(fase.get("duration"))>9:
                    self.fases.append(float(fase.get("duration")))
## Extraccion de acciones posibles. Estas vienen dadas en forma de lista, donde cada entrada es un diccionario, el cual contine 
# suma que se le tiene que hacer a cada fase, menos a la ultima.
   def Acciones(self,margen):
       dic1={}
       a=self.Listaytipo()
       del a[self.ultimoestado]
       ok=a
       for p in margen:
           for b in ok:
               ok[b]=[ok[b],[-p,0,p]]
           self.accio=ok
           self.acciored={}
           l=0
           m=0
           o=l
           op=pow(3,len(self.accio))-1
           opb3=len(np.base_repr(op,base=3))
           b1={}
           v=[]
           lacc=list(self.accio)
           for i in range(op+1):
               k=np.base_repr(i,base=3,padding=opb3-len(np.base_repr(i,base=3))+2)[::-1]
               for j in range(opb3):
                   b1.update({lacc[j]:[-p,0,p][int(k[j])]  })           
               v.append(b1)
           dic1.update({str(p):v})
           b1={}
       self.accionred=dic1 
       # v=list(self.accio)
       # for i in range(op+1):
           
       #     k=np.base_repr(i,base=3,padding=opb3-len(np.base_repr(i,base=3)))
       #     for j in range(opb3):
       #         b.update({v[j]:[-0.5,0,0.5][int(k[j])]  })
               
            
       #     self.acciored.update({i:b})
       #     b={}

           
## Cambiar el estado. Primero z                
   def Efectuaraccion(self):
       x=0
       a=[]
       for i in self.accio:
           if i=="offset":
               q=self.accionelegida[i]               
           else:
               q=self.accionelegida[i]
               x=x+q 
               a.append((q+self.estado[i]))
       a.append(self.estado[self.ultimoestado]-x) 

       
       if all(i >= 10 for i in a):
           x=0
           for i in self.accio:
               if i=="offset":
                   q=self.accionelegida[i]
                   self.estado[i]=((q+self.estado[i]) % self.ciclo)
               else:
                   q=self.accionelegida[i]
                   x=x+q
                   self.estado[i]=((q+self.estado[i]))
           self.estado[self.ultimoestado]=self.estado[self.ultimoestado]-x 
       
            # Posible bug numeros negativos.
   def select_action(self,margen):
       if rate > random.random():
           self.action=randrange(len(self.accionred[str(margen)]))
           self.accionelegida=self.accionred[str(margen)][self.action] # explore      
       else:
           with torch.no_grad():
               self.action=int(Redes[str(margen)][self.nombre].forward(torch.FloatTensor([self.fases]).to(device)).argmax())
               self.accionelegida=self.accionred[str(margen)][self.action] # exploit
               #self.accionelegida=self.acciored[int(policy_net(state).argmax(dim=1).to(self.device)[0])] # exploit
       
   # def select_action(self,margen):
   #     for i in margen:
   #         if rate > random.random():
   #             self.action=randrange(len(self.accionred[str(i)]))
   #             self.accionelegida=self.accionred[self.action]
   #         else:
   #             with torch.no_grad():
   #                 self.action=int(self.forward(torch.FloatTensor([self.fases]).to(device)).argmax())
   #                 self.accionelegida=self.accionred[self.action] # exploit
   #             #self.accionelegida=self.acciored[int(policy_net(state).argmax(dim=1).to(self.device)[0])] # exploit
   def inicio(self, strategy, num_actions, device):
       self.current_step = 0
       self.strategy = strategy
       self.num_actions = num_actions
       self.device = device
                 
   def get_options():
      optParser = optparse.OptionParser()
      optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
      options, args = optParser.parse_args()
      return options   
    
   def Training(self):
       self.select_action()
       self.Efectuaraccion()
       
   def Carreteras(self):
       self.aristas=[]
       for i in self.vinc:
           self.aristas.append(dedges.at[dedges.loc[(dedges["from"]==i)&(dedges["to"]==self.nombre)].index[0],"id"])
           
   def Tiempo_espera_interseccion(self):
       for i in self.aristas:
           self.tiempoespera+=traci.edge.getWaitingTime(i)
           
           
   def Recompensa(self):
       self.recom=-self.tiempoespera
       for i in self.vinc:
           self.recom+=-Sems[i].tiempoespera/len(self.vinc)
       for i in self.vout:
           self.recom+=-Sems[i].tiempoespera/len(self.vout)
       return self.recom
       
def sumo1():
    
     if __name__ == "__main__":
         options = get_options()
     
         # this script has been called from the command line. It will start sumo as a
         # server, then connect and run
         if options.nogui:
             sumoBinary = checkBinary('sumo')
         else:
             sumoBinary = checkBinary('sumo-gui')
     
         # first, generate the route file for this simulation
     
         # this is the normal way of using traci. sumo is started as a
         # subprocess and then the python script connects and runs
         traci.start(["sumo", "-c", "C:/Users/juano/Documents/Tfm/Tfm/sim.sumocfg",
                                  "--tripinfo-output", "tripinfo.xml"])
         step = 0
         while 250 > step:
             traci.simulationStep()
             for i in df_tl["id"]:
                 Sems[i].Tiempo_espera_interseccion()
     
             step += 1
         traci.close()

def sumo():
     if __name__ == "__main__":
         options = get_options()
     
         # this script has been called from the command line. It will start sumo as a
         # server, then connect and run
         if options.nogui:
             print("nogui")
             sumoBinary = checkBinary('sumo')
         else:
             print("gui")
             sumoBinary = checkBinary('sumo-gui')
     
         # first, generate the route file for this simulation
     
         # this is the normal way of using traci. sumo is started as a
         # subprocess and then the python script connects and runs
         traci.start(["sumo", "-c", "/home/juan/Documentos/Tfm/sim.sumocfg",
                                  "--tripinfo-output", "tripinfo.xml"])
         step = 0
         while 700> step:
             traci.simulationStep()
             for i in df_tl["id"]:
                 Sems[i].Tiempo_espera_interseccion()
     
             step += 1
     traci.close()
# %%
Sems={}
Tar1={}
Tar2={}
Red1={}
Red2={}
Redes={}
Targets={}
Paccion=[0.5,3]
for i in df_tl["id"]:
    a='./tlLogic[@id="'+i+'"]'
    Sems.update({i:Extr(root.findall(a)[0],Paccion).to(device)})
    Tar1.update({i:Target(len(Sems[i].fases),len(Sems[i].accionred[str(Paccion[0])])).to(device)})
    Tar2.update({i:Target(len(Sems[i].fases),len(Sems[i].accionred[str(Paccion[1])])).to(device)})
    Red1.update({i:Target(len(Sems[i].fases),len(Sems[i].accionred[str(Paccion[0])])).to(device)})
    Red2.update({i:Target(len(Sems[i].fases),len(Sems[i].accionred[str(Paccion[1])])).to(device)})

Redes.update({str(Paccion[0]):Red1})
Redes.update({str(Paccion[1]):Red2})
Targets.update({str(Paccion[0]):Tar1})
Targets.update({str(Paccion[1]):Tar2})
       
