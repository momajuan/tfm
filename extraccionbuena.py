# %%
import os
import pandas as pd
import numpy as np
from xml.etree import ElementTree as ET
# Import required package
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
import networkx as nx
from networkx.algorithms.community import greedy_modularity_communities
import matplotlib.pyplot as plt 


# %%
file_name='Mapa1.net.xml'
dom=ET.parse(file_name)
root = dom.getroot()
df=pd.DataFrame(columns=['id','x','y'])
junction=pd.DataFrame(columns=['id','x','y'])

print(root.tag)
# %%
## Junctions
v=root.findall('./junction[@shape]')
for elm in v:
    junction=junction.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
print(junction)

## Traffic light
v=root.findall('./junction[@type="traffic_light"]')
for elm in v:
    df=df.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
print(df)
df_tl=df.copy()
for elm in root.findall('./edge[@to]'):
    if df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty  == False:
        df=df.append({'id': elm.attrib["from"]}, ignore_index=True)
df=df.drop_duplicates(subset=['id'], keep='first')
df=df.reset_index()
print(df)




n=df.shape[0]
# %%
##Añadir x, y
for i in list(df.id):
    df.loc[df['id']==i,'x':'y']=junction.loc[junction['id']==i,'x'].iloc[0],junction.loc[junction['id']==i,'y'].iloc[0]
## Matriz de Conexion de intersecciones
JM = csr_matrix((n, n),dtype = np.int8).toarray()
## Matriz de conexion con numero de carriles
JC = csr_matrix((n, n),dtype = np.int8).toarray()
## Matriz de posicion de semaforo en la intersecciones
JS = csr_matrix((n, n),dtype = np.int8).toarray()
## Matriz de longitud de segmento
JL = csr_matrix((n, n),dtype = np.float).toarray()
## Matriz de velocidad
JV = csr_matrix((n, n),dtype = np.float).toarray()
## Matriz de velocidad
JI = csr_matrix((n, n),dtype = np.float).toarray()
print(JM)
JM=coo_matrix(JM)
JC=coo_matrix(JC)
JS=coo_matrix(JS)
JL=coo_matrix(JL)
JV=coo_matrix(JV)
JI=coo_matrix(JI)
jm=JM.tocsr()
js=JS.tocsr()
jl=JL.tocsr()
jv=JV.tocsr()
ji=JI.tocsr()
jc=JC.tocsr()
dedges=pd.DataFrame(columns=['id','from','to','index'])
print(root.tag)
for elm in root.findall('./edge[@from]'):
    dedges=dedges.append({'id': elm.attrib["id"],'from':elm.attrib["from"],'to':elm.attrib["to"],'index':[]}, ignore_index=True)
print(dedges)
# %%
for elm in root.findall('./edge[@to]'):
    if df.loc[df['id'] == elm.attrib["from"]].empty == False and df.loc[df['id'] == elm.attrib["to"]].empty==False:
        print(1)
        print(elm)
        jm[ df.loc[df['id']==elm.attrib["from"]].index[0],df.loc[df['id']==elm.attrib["to"]].index[0]]=1
        # js[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
        # jl[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
        # jv[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
        # jc[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())

# %%
## Aristas, de donde viene a donde van y indice de semaforo que las une
for elm in root.findall('./connection[@tl]'):
    print(elm.attrib["linkIndex"])
    dedges.iloc[dedges.iloc[(dedges['id'] == elm.attrib["from"]).values,[3]].index[0]][3].append(elm.attrib["linkIndex"])

# %%
###
dedges.iloc[1][3].append(1)
print(dedges.loc[dedges['id'] == elm.attrib["from"],['id']])
print(dedges.iloc[(dedges['id'] == elm.attrib["from"]).values,[1]].index[0])
print(dedges)
dedges.iloc[dedges.iloc[(dedges['id'] == elm.attrib["from"]).values,[3]].index[0]][3].append(elm.attrib["linkIndex"])
print(elm.attrib)
print(dedges)

# %%
## Diccionario posicion 
o1={x: np.array([float(df.iloc[x,2]),float(df.iloc[x,3])]) for x in list(range(0,n))}

##### GRAFO SIN PESO
## Medidas del grafo.
G=nx.from_scipy_sparse_matrix(jm, parallel_edges=False, create_using=nx.DiGraph)
# %%
pr = nx.pagerank(G, alpha=0.9)

c = list(greedy_modularity_communities(G))
print(c[5])
print(G.adjacency())
A1 = nx.to_scipy_sparse_matrix(G)
# %%
h, a = nx.hits(G,max_iter=1000)
bc=nx.betweenness_centrality(G, k=None, normalized=True, weight=None, endpoints=False, seed=None)

## a
a1 = np.array(list(a.values()))
sorted(range(len(a1)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(a1)), key=lambda i: a[i], reverse=True)[:5]:
    a1[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=a1*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=2)
plt.show()
# %%

## Hubs Dibujo 5 mejores
hubs = np.array(list(h.values()))
sorted(range(len(hubs)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(hubs)), key=lambda i: a[i], reverse=True)[:5]:
    hubs[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=hubs*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()

## pagerang Dibujo 5 mejores
pagerank = np.array(list(pr.values()))
sorted(range(len(pagerank)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(pagerank)), key=lambda i: a[i], reverse=True)[:5]:
    pagerank[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=pagerank*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()

## Betweeness 
## Hubs Dibujo 5 mejores
bc1 = np.array(list(h.values()))
sorted(range(len(bc1)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(bc1)), key=lambda i: a[i], reverse=True)[:6]:
    bc1[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=bc1*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()




##### GRAFO CON PESO
## Medidas del grafo.
G=nx.from_scipy_sparse_matrix(jc, parallel_edges=False, create_using=nx.DiGraph)
pr = nx.pagerank(G, alpha=0.9)
c = list(greedy_modularity_communities(G))
print(c[5])
print(G.adjacency())
A1 = nx.to_scipy_sparse_matrix(G)
print(A1!=jm)
h, a = nx.hits(G,max_iter=1000)
bc=nx.betweenness_centrality(G, k=None, normalized=True, weight=None, endpoints=False, seed=None)

## a
a1 = np.array(list(a.values()))
sorted(range(len(a1)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(a1)), key=lambda i: a[i], reverse=True)[:5]:
    a1[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=a1*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()

## Hubs Dibujo 5 mejores
hubs = np.array(list(h.values()))
sorted(range(len(hubs)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(hubs)), key=lambda i: a[i], reverse=True)[:5]:
    hubs[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=hubs*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()

## pagerang Dibujo 5 mejores
pagerank = np.array(list(pr.values()))
sorted(range(len(pagerank)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(pagerank)), key=lambda i: a[i], reverse=True)[:5]:
    pagerank[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=pagerank*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()

## Betweeness 
## Hubs Dibujo 5 mejores
bc1 = np.array(list(h.values()))
sorted(range(len(bc1)), key=lambda i: a[i], reverse=True)[:5]
for i in sorted(range(len(bc1)), key=lambda i: a[i], reverse=True)[:5]:
    bc1[i]=+0.5
nx.draw_networkx_nodes(G,pos=o1, node_size=bc1*1000+1)
nx.edges = nx.draw_networkx_edges(G, pos=o1,width=10)
plt.show()
